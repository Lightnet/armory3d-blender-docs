

# Scripts:

## Script API:
 Using haxe script to setup as Logic node tree. The package that connect to blender for haxe is iron.
 
 > Note: But please remember that the script has to module design to be reused able.

MyTrait.hx
```haxe
package arm;
class MyTrait extends iron.Trait {
	public function new() {
		super();

		// notifyOnInit(function() {
		// });

		// notifyOnUpdate(function() {
		// });

		// notifyOnRemove(function() {
		// });
	}
}
```
 Default setup for haxe logic node tree component.

```haxe
package arm;
```
This is for haxe script use as component for blender node tree only access here to get enitity running correctly. It was has to be place in arm folder and Armory3D create default path when blender create new haxe script.

 > Note some of the scripts are from Armory3d github example projects.

## Script node props access:
 Very simple add prefix to @prop to able to display in Armory Traits if you have haxe script select and assign.

```haxe
 @prop public var speed:Float = 0.1;
```
 This will display your variable but you need to refresh to update params. That should be under Armory Traits Panel.


## Global Variables:
 Note that variable must not be null that should be default variable else it will still null when assign from Armory Traits prop blender panel.

Config.hx
```haxe
package arm;

class Config extends iron.Trait {

    @prop public var speed:Float = 0.1;
    @prop public var text:String = "";

    public static var inst:Config = null;

    public function new() {
        super();
        inst = this;
    }
}
```
 This will display your variable but you need to refresh to update params. That should be under Armory Traits Panel.

```haxe
 @prop public var speed:String;
```
 This does not work. It need be assign as default string name or "" to make sure the null is not used as default.

ReadConfig.hx
```haxe
package arm;

class ReadConfig extends iron.Trait {
	public function new() {
		super();

		notifyOnInit(function() {
			var c = Config.inst;
			//trace("Text is: " + c.text + ", speed is: " + c.speed);
			trace("Text is: " + c.text);
			//trace("Speed is: " + c.speed);
		});
	}
}
```

![alt text](../images/blenderarmorytraits.png "Armory Traits")

Here the image how it setup. Class is 'Config' is haxe script. Note you need to have debug in Render Panel > Armory Project > Display Console Checked.

### Link:
  * https://github.com/armory3d/armory/issues/312 (Global variables)

## Armory Packages
```
import armory.trait.physics.PhysicsWorld;
import iron.system.Input;
import iron.math.Vec4;
```

## Get Input Key
```haxe
package arm;

import iron.math.Vec4;

class LockTrait extends iron.Trait {
	public function new() {
		super();

		notifyOnUpdate(function() {
			var mouse = iron.system.Input.getMouse();

			if (mouse.started("left")) {
				//mouse.lock();
				trace("left");
			}
			else if (mouse.started("right")) {
				//mouse.unlock();
				trace("right");
			}
		});
	}
}

```

## Get Scene Object 

```
var cube = iron.Scene.active.getChild("Cube");
```
 Active Object

```
var tr = iron.Scene.active.getChild("Goal").transform;
```
 Active transform


## Animation

```
import iron.object.BoneAnimation;
var anim = cast(object.children[0].animation, BoneAnimation);
```

 > Note that Armature with mesh will work with animation. If just armature object only result in error if there no mesh parent from armature.
