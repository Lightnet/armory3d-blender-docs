---
layout: page
title: licenses
date: 2018-06-07 16:12:26
tags:
---

Work in progress.

# Armory 3D
 http://armory3d.org/

## Features
 * Armory3D is integration into blender APIs to build and run game engine.(Blender 2.80)
 * Cycles, Eevee, and Armory can be used as render game engine.
 * Kha Cross-Platform Tool can be export with out need to write configs builds that is added into armry3d blender render settings can be setup or scripted.
 * Use Haxe (.hx) script as components node for object in the scene to have functions as node tree.

## Game Engine:
 * Rendering
 * Physics
 * Audio
 * Navigation
 * Scripting
 * Networking

# Check out for more information:
 * http://armory3d.org/

## Information
 Every thing can be build from logic nodes and node trees. Think of as an node graph charts using visual blocks that connect to other functions or variables. Or you can code by using haxe and other code languages that is support by Armory3D.

## Scripts
 * Haxe
 * Python
 * Javascript
 * Logic nodes (Blender)

## Deploying
 Armory3D Use Kha cross-platform tool to export and build the application.

### Features
  * Hardware Abstaction
  * No dependency
  * No Bloat
  * No (Graphics) API specifics
  * No recompilation

## Docs
 * [Haxe Script API](haxeapiscripts.html)
 * [Blender Node Tree](blendernodetree.html)

## Get Started

 Those are users create contents.

 * https://www.youtube.com/watch?v=JcLBfb6EBhA (Armory 3D Tutorial Getting Started)
 * https://www.youtube.com/watch?v=oR0itVswATg (Armory 3D Character Animation Tutorial)
 * https://www.youtube.com/watch?v=-NHmJMNy1lM (Armory 3D Engine Tutorial : Node Trees)

 * https://www.youtube.com/watch?v=43k09de9Mb0 (LIVENODING 1024 / Armory Debug Print and Random Nodes)



## Links
 * https://www.youtube.com/watch?v=EaMT6Nyu79w (Armory Real Time 3D Engine in Blender)
 * https://github.com/armory3d/armory_docs/blob/master/getting_started/playground.md