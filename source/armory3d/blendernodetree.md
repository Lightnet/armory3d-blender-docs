# Armory3D Blender API
 There are basic set up node and game relate to logic node trees to add to game engine to the scene.

## Armory3D Blender Node Tree
 Armory3D has added node tree functions to like in haxe function API build for game engine logics.

![alt text](../images/blenderlogicnodetree.png "Logic Node Tree")

Blender Logic Node Tree

## Armory3D Node Tree API
 * Event
   * On Init
   * On Update
   * On Event
   * On Timer
   * On Volume Trigger
 * Add Trait
 * Spawn Object
 
 ## Preset
  There are armory preset build into blender to have features to have game engine like terrain, physics, and other relation to game creating world. Blender scene object need to havr armory node component for game to work. Example game triggers, lights, camera and other things. 
  
  > Need to list them to get the docs to detail.