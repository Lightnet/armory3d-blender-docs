---
title: Armory3D Blender Docs
---

Work in progress.

Date: 2018-06-07

# Blender 2.80

# Armory3D Blender Docs

This project is for a documentation website using [Hexo](https://hexo.io) and [hexo-theme-doc](https://github.com/zalando-incubator/hexo-theme-doc).   

## Information
 I am not part of Armory3D Staff or official documentation. This is just documentation guide, references builds, and other things to help improve information to build the game or application like architect tour guide for example.

 Armory3D is 3D game engine with blender as 3D model integration into application. It used Haxe and Kha cross-platform tools, code language, export type of platform. Like mobile, window, mac, linux and browser as well others.

## How it works?
  You can read more about Armory3D web site. It is has tools build or to say add on scripts with Blender 3D modeling. To able to run debug game or application and export to binary or to browser application. It used Haxe code language to build functions and variable as component to added to entity object like scene objects. Kha is cross-platform tools compile base on haxe and run application tool for exporting type of operating systems. You can config build type of os or browser in render settings under Armory Properties.

 Why it Kha and not haxe export tool? Kha is base on haxe to build better export tool. Read More on [Kha](http://kha.tech).

 There is little bit of javascript and nodejs. But those are config and builds. Have not check more information about it. The file script predefine name to have entry point for main to access point.

``` 
 khafile.js
```
 Main Entry start. It for kha to compile as config build.


## Getting Started

 You can download [Armory3D](https://armory.itch.io/armory3d) SDK.

 Once download the platform. Extract file and run Blender. Create folder project since by default it creates a project files to run applicaiton once blender save at root project. Then create blender file and save it. Reason is simple that it need to build applcation and file to compile application. F5 Key to run game. Or go to scene render panel. It on right panel Camera icon and select in data properties editor type. Look for Amory Player. Click Play button. By default it have some movement control camera. WASD = movement.

 Please note it take a while to build and run the application.

 You can look up the console log on the Top menu bar. Window > Toggle System Console

 Notes: Blender 2.80 doesn't have some features ready yet in some testing unless the example projects are working from Armory3D github.

### Links:
 There are sample files for Armory3D projects.
 * https://github.com/armory3d
 * https://github.com/armory3d/armory_examples

### Users Video:

## More Informations
 * http://armory3d.org
 * https://armory.itch.io
 * https://armory.itch.io/armory3d

## Other Applications:
 * https://github.com/armory3d/armorpaint

## License

The MIT License (MIT)