---
layout: page
title: ide
date: 2018-06-07 16:13:46
tags:
---

# Information:
 Note I have try few IDE's that tested but not yet full tested. Reason is config usually take time to config test build.

# IDE's
 * [Visual Studio Code](https://code.visualstudio.com) 1.24.0 Last Updated: 06-06-2018
 * [Kode Studio](https://github.com/Kode/KodeStudio) 5.3.3 Last Updated: 23-09-2017
 * [FlashDevelop](http://www.flashdevelop.org) 5.3.3 Last Updated: 28-02-2018
 * [HaxeDevelop](https://haxedevelop.org) 5.3.3 Last Updated: 28-02-2018
 * [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/)  Last Updated:  2018

## Visual Studio Code

 [Visual Studio Code Site](https://code.visualstudio.com)

 Install extension 'Kha Extension pack' to run kha cross compiler. Create a folder project. F1 key to search for Init Kha Project.

 F5 Key to start debug mode default [electionjs browser](https://electronjs.org/) application.


## Kode Studio

 [Kode Studio Site](https://github.com/Kode/KodeStudio)

 Create a folder project. F1 key to search for Init Kha Project.

 F5 Key to start debug mode default [electionjs browser](https://electronjs.org/) application.

## IntelliJ IDEA Community Edition

 [IntelliJ IDEA Community Edition Site](https://www.jetbrains.com/idea/)

 Install Haxe plugin.
