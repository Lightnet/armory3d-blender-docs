---
layout: page
title: kha
date: 2018-06-07 16:11:54
tags:
---

# Kha
 
 [kha Site](http://kha.tech)

## TRUE CROSS-PLATFORM
 > Kha can cross-compile your code and optimize your assets for even the most obscure systems. Build your applications natively to desktops, tablets, phones and consoles. Implement once, run everywhere.

## KODE STUDIO

 > Kha has its own IDE. Available for Windows, macOS and Linux, it has powerful features as IntelliSense, built-in Git and debugging right from the editor.

## FAST AND OPEN SOURCE
 > Great performance on each platform and all the [source code](https://github.com/Kode/Kha), all the time - no black boxes

 Quotes [Kha site](http://kha.tech).

## Information:
  > Kha is a low level sdk for building games and media applications in a portable way. Think SDL, but super-charged. Based on the Haxe programming language and the krafix shader-compiler it can cross-compile your code and optimize your assets for even the most obscure systems. Kha is so portable, it can in fact run on top of other game engines and its generational graphics and audio api design gets the best out of every target, supporting super fast 2D graphics just as well as high end 3D graphics. Also, Kha probably added a new target while you were reading this text.

 Quote from Github [Kha](https://github.com/Kode/Kha)

## Get Started

## Wiki:

## Examples:

## License

Copyright (c) 2017 the Kha Development Team

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.