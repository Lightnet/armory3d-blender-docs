# Armory3D-Blender-Docs

This project is for a documentation website using [Hexo](https://hexo.io) and [hexo-theme-doc](https://github.com/zalando-incubator/hexo-theme-doc).   

## Information
 I am not part of Armory3D Staff or official documentation. This is just documentation guide, references builds, and other things to help improve information to build the game or application like architect tour guide for example.

 Armory3D is 3D game engine with blender as 3D model build into application. It used Haxe cross-platform tools and code language with Kha cross-platform tools add ons.

## Getting Started

 You can download [Armory3D](https://armory.itch.io/armory3d) SDK.

## More Informations
 * http://armory3d.org
 * https://armory.itch.io
 * https://armory.itch.io/armory3d

## License

The MIT License (MIT)